-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Apr 2022 pada 15.59
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si_zoom_uas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_01_02_124305_create_stafs_table', 1),
(6, '2022_01_02_132230_create_zooms_table', 1),
(7, '2022_01_03_004909_add_deleted_at_to_zoom_table', 1),
(8, '2022_01_04_145251_create_peminjaman_table', 1),
(9, '2022_01_06_104033_add_deleted_at_to_peminjaman_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `reset_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`id`, `user_id`, `reset_code`, `created_at`, `updated_at`) VALUES
(1, 1, 'zhmaPDu3cIEbnp0mKSIy41sMUAdHuWIV227sCM8NXS0WSIS5oHHQcy8Rq51VmSEavLCE5BzgvfULrAXyGmxU5uJJyS8EFbTUyEUk', '2022-01-07 03:13:33', '2022-01-07 03:13:33'),
(2, 1, 'gVaEKZuWpHOfMjpdNyAKg7vlk0Cb3cnLFkByGkehABfLts4xYRCcsS8IzAJtebbjXcFMrtI3ItZIhzO2epJytQlXGWoJFcIBE6Bu', '2022-01-07 03:37:32', '2022-01-07 03:37:32'),
(3, 1, 'rXUCPXAtnEnAUxSysUxiIvsyWKMlD1lnPwCnhF7qMTgLzx5sDVOh8M3Qj8tWefBEW1v8jBrRz0TZK0I23U8ITu3o9tiQEgl3IEt1', '2022-01-07 03:39:51', '2022-01-07 03:39:51'),
(4, 1, '1v8vlHAofx5pwJytBKObLXDtMUp8UhnnVei7UHNqNevW5taoolCXxCgwMA5BtsUgv50vfxuidpO3TrkbiuoFrJeWhpyOd3ZCD0ao', '2022-01-07 03:40:36', '2022-01-07 03:40:36'),
(5, 1, '5pcRLAu5UEjiQ5JKFyI8g0FSRI111rMbDlWg8fCOrJ1fh5TQIHDx5FnLQjT1dnE8a4NLUBmLnLvXqfbcNa0aLmZNOupIRbwCdNkW', '2022-01-07 03:41:40', '2022-01-07 03:41:40'),
(6, 1, 'ZziO5RuXBVdgzCTB8it4rfDJsWbCCICuRjYSDxmyO4SQAAF9vyDLROOI0agueEr3pZHCHT0AIyzPG1p3zA1H7PKzdzy94PJvu9u6', '2022-01-07 04:05:28', '2022-01-07 04:05:28'),
(7, 1, 'vVLI07KJKZ5MAED15XCWLuQ2mgFFZm5f21AiGy2WWm9rjFMmEL75cEaajINRslQ3HqLrjbJmGoh1TwzNP2VhFbfLqZqBj1r0ilhc', '2022-01-07 04:10:47', '2022-01-07 04:10:47'),
(8, 1, 'PKMRsxYFbyw22Q71oQ7kzY7TS1z60cQo02yrZwwrSSDqsFq2V3rUBx986XdL05MC3tR1N29iH66xjwKQkYyLnD5FRtdUeuumDU1U', '2022-01-07 04:37:04', '2022-01-07 04:37:04'),
(9, 1, 'HItljeG8jntEKaalrhSh9pPAoiqSyOUYeJEOZYF2I3HLYp6k0hlBLZ3zpwH5ReT79Et2S2J2e3qRJ6FButELKz7zEETmxf0JYA1f', '2022-01-07 04:46:59', '2022-01-07 04:46:59'),
(10, 1, 'wOuGdwxOHmp5XTOtswcSQVHp1PFS1shHuNpday5FQqzFXCmqGYDHo5kmQyN4vvpk8Q6K6lmhXpjo3CuH1R2KGukoF9MdE2Mj7w60', '2022-01-07 05:00:57', '2022-01-07 05:00:57'),
(11, 1, 'DzlGtFIGMf5n25SpFS0wO6QeyXhX31cUXnDGgWuoSWDFlehCihbShAcLUWCWA0jvUrgGYUT9yCIPqHxHS0iTnF1AXGle4CSgM3fe', '2022-01-07 05:18:25', '2022-01-07 05:18:25'),
(13, 2, 'ItcuWoqAoveZzj0Fgdftu4EMCDoloYygtDvodXlPtFOsQWxjT605Lg6BlnQA4wZS0nsxrpXjV5pk7iuCS1xrPiv7h4Q2QkooiX0Z', '2022-01-07 05:42:50', '2022-01-07 05:42:50'),
(14, 2, 'QNpPmwv3V8DkLy1i1xc3Rk5DINb3BfjWJglfnwEJ0AtlMwNnalRWcm4DIGClMNUXIKsNH8aLlsoqFNAPRMJ6fhRxgV79nh7yV1ao', '2022-01-07 05:43:52', '2022-01-07 05:43:52'),
(15, 1, 'uQvJBp7QNPRdxTXSFR2MEDkYz079z4x8qmkOaAjl8GKwIw3PbUKgNzcrafkn2PchokzPn7vhDETohtMz1GrDiVRmiHBsvnLOiwBG', '2022-01-07 05:44:34', '2022-01-07 05:44:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_zoom` bigint(20) NOT NULL,
  `nama_kegiatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status` enum('approved','rejected','dibatalkan','selesai') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `id_zoom`, `nama_kegiatan`, `deskripsi`, `tanggal_pinjam`, `tanggal_kembali`, `status`, `catatan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Webinar', 'test', '2022-01-08 09:26:00', '2022-01-09 09:27:00', 'dibatalkan', 'link akun zoom', '2022-01-07 17:27:11', '2022-01-07 21:47:37', '2022-01-07 21:47:37'),
(2, 3, 'Webinar 2', 'test', '2022-01-08 09:27:00', '2022-01-08 09:27:00', 'approved', 'Link Zoom', '2022-01-07 17:27:43', '2022-01-07 20:56:57', NULL),
(3, 5, 'Yudisium', 'test', '2022-01-07 12:00:00', '2022-01-07 15:00:00', NULL, NULL, '2022-01-07 20:55:00', '2022-01-07 20:55:00', NULL),
(4, 6, 'Webinar Kepemimpinan', 'test', '2022-01-06 12:00:00', '2022-01-07 12:00:00', 'selesai', 'selesai', '2022-01-07 20:55:54', '2022-01-07 21:54:51', NULL),
(5, 7, 'Webinar', 'test', '2022-01-07 13:46:00', '2022-01-07 15:46:00', NULL, NULL, '2022-01-07 21:46:36', '2022-01-07 21:51:09', '2022-01-07 21:51:09'),
(6, 7, 'Webinar', 'test', '2022-01-08 13:50:00', '2022-01-08 15:50:00', 'dibatalkan', 'dibatalkan karena ...', '2022-01-07 21:50:59', '2022-01-07 21:53:53', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stafs`
--

CREATE TABLE `stafs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `role` tinyint(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(10, 'Dita', 'ditaoktaviari10@gmail.com', NULL, 0, '$2y$10$oksNxiWk7NUd0kZj.10aAeeSW23pPYR0b8QSD17hsrPDzo2ORpuiq', NULL, '2022-01-07 21:36:21', '2022-01-07 21:40:05'),
(11, 'Dita', 'ditaoktaviari@gmail.com', NULL, 1, '$2y$10$Dg8.eKXVlxTZ1NmpLzHzjevsIEs2IPdbueJVzxiMmi8joJgm1xm4W', NULL, '2022-01-07 21:37:17', '2022-01-07 21:37:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `zoom`
--

CREATE TABLE `zoom` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_akun` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `status_aktif` enum('aktif','tidak aktif') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `zoom`
--

INSERT INTO `zoom` (`id`, `nama_akun`, `kapasitas`, `status_aktif`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test', 100, 'tidak aktif', '2022-01-07 06:26:56', '2022-01-07 17:05:39', '2022-01-07 17:05:39'),
(2, 'test', 100, 'aktif', '2022-01-07 06:28:47', '2022-01-07 17:05:42', '2022-01-07 17:05:42'),
(3, 'Fakultas 1', 100, 'aktif', '2022-01-07 17:06:01', '2022-01-07 17:06:01', NULL),
(4, 'Fakultas 2', 1000, 'tidak aktif', '2022-01-07 17:06:40', '2022-01-07 17:06:40', NULL),
(5, 'Fakultas 3', 500, 'aktif', '2022-01-07 20:52:12', '2022-01-07 20:52:12', NULL),
(6, 'Fakultas 4', 100, 'aktif', '2022-01-07 20:53:37', '2022-01-07 20:53:37', NULL),
(7, 'Fakultas 5', 100, 'aktif', '2022-01-07 21:42:19', '2022-01-07 21:42:19', NULL),
(8, 'Fakultas 7', 100, 'aktif', '2022-01-07 21:42:51', '2022-01-07 21:43:32', '2022-01-07 21:43:32');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `stafs`
--
ALTER TABLE `stafs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `zoom`
--
ALTER TABLE `zoom`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `stafs`
--
ALTER TABLE `stafs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `zoom`
--
ALTER TABLE `zoom`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
