@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        Data Peminjaman
                    </h3>
                </div>
            </div>
        </div>

        <form method="post"
            action="{{ route('peminjamanUpdate', ['id' => $edit->id]) }}"
            class="form-send m-form m-form--fit m-form--label-align-right"
            enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-grid-menu-v2"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Edit Peminjaman
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        {{-- @if(Auth()->user()->role == 0){
                        <div class="form-group m-form__group">
                            <label>Nama Akun Zoom</label>
                            <select name="id_zoom" class="form-control m-input">
                                @foreach ($zoom as $row)
                                    <option value="{{ $row->id }}" {{ $row->id == $edit->id_zoom ? 'selected':'' }}>{{ $row->nama_akun.' - '.$row->kapasitas }} orang</option>
                                @endforeach
                            </select>
                            @error('id_zoom')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Nama Kegiatan</label>
                            <input type="text" name="nama_kegiatan" class="@error('nama_kegiatan') is-invalid @enderror form-control m-input" required value="{{ $edit->nama_kegiatan }}">
                            @error('nama_kegiatan')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" class="@error('deskripsi') is-invalid @enderror form-control m-input" required value="{{ $edit->deskripsi }}"></textarea>
                            @error('deskripsi')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Pinjam</label>
                            <input type="datetime-local" name="tanggal_pinjam" class="@error('tanggal_pinjam') is-invalid @enderror form-control m-input" required value="{{ $edit->tanggal_pinjam }}">
                            @error('tanggal_pinjam')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Tanggal Kembali</label>
                            <input type="datetime-local" name="tanggal_kembali" class="@error('tanggal_kembali') is-invalid @enderror form-control m-input" required value="{{ $edit->tanggal_kembali }}">
                            @error('tanggal_kembali')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        @endif --}}
                        <div class="form-group m-form__group">
                            <label>Status</label>

                            @php
                                $kembali = strtotime($edit->tanggal_kembali);
                                $now = date("Y-m-d H:i:s");
                                $now2 = strtotime($now);
                                $selisih = $now2 - $kembali;
                            @endphp

                            <select name="status" id="exampleSelect1" class="@error('status') is-invalid @enderror  form-control m-input m-input--square" required>
                                <option value="">Pilih Status</option>
                                <option value="approved" {{ $edit->status == 'approved' ? 'selected':'' }}>Approved</option>
                                <option value="rejected" {{ $edit->status == 'rejected' ? 'selected':'' }}>Rejected</option>
                                @if($edit->status == 'approved')
                                    <option value="dibatalkan" {{ $edit->status == 'dibatalkan' ? 'selected':'' }}>Dibatalkan</option>
                                @endif
                                @if($selisih >= 0)
                                    <option value="selesai" {{ $edit->status == 'selesai' ? 'selected':'' }}>Selesai</option>
                                @endif
                            </select>
                            @error('status')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group m-form__group">
                            <label>Catatan</label>
                            <textarea name="catatan" class="@error('catatan') is-invalid @enderror form-control m-input" required>
                                {{ $edit->catatan }}
                            </textarea>
                            @error('catatan')
                                <div class="form-control-feedback">
                                    <span style="color:red">
                                        {{ $message }}
                                    </span>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="m-portlet akses-list">
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions text-center">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <a href="{{ route('peminjamanList') }}" class="btn btn-secondary">Batal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </form>
    </div>
@endsection