@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $(document).on('click', '#detail', function(){
                var namaakun = $(this).data('namaakun');
                var catatan = $(this).data('catatan');
                $('#namaakun').text(namaakun);
                $('#catatan').text(catatan);
            })
        })
    </script>
@endsection

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            @if (session('msg'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    {{ session('msg') }}
                </div>
            @elseif (session('sukses'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                    {{ session('sukses') }}
                </div>
            @endif
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Daftar Peminjaman Approved
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet akses-list">
                <div class="m-portlet__body">
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m_datatable" id="local_data">
                        <div class="table-responsive">
                            <table class="akses-list table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="20">No</th>
                                        <th width="20">Nama Akun Zoom</th>
                                        <th width="20">Nama Kegiatan</th>
                                        <th width="20">Deskripsi</th>
                                        <th width="20">Tanggal Pinjam</th>
                                        <th width="20">Tanggal Kembali</th>
                                        <th width="20">Durasi</th>
                                        <th width="20">Catatan</th>
                                        <th width="20">Status</th>
                                        <th width="20">Menu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data_peminjaman as $data)
                                        @php
                                           $durasi = strtotime($data->tanggal_kembali) - strtotime($data->tanggal_pinjam);
                                           $jam = $durasi / 3600 % 24;
                                           $menit = $durasi / 60 % 60;
                                           $detik = $durasi % 60;
                                        @endphp
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->nama_akun }}</td>
                                            <td>{{ $data->nama_kegiatan }}</td>
                                            <td>{{ $data->deskripsi }}</td>
                                            <td>{{ $data->tanggal_pinjam }}</td>
                                            <td>{{ $data->tanggal_kembali }}</td>
                                            <td>{{ $jam. ' jam '.$menit.' menit '.$detik.' detik '}}</td>
                                            <td>{{ $data->catatan }}</td>
                                            <td>
                                                @if($data->status != null)
                                                    @if($data->status == 'approved')
                                                        <button class="btn m-btn--pill btn-primary btn-sm m-btn m-btn--custom">
                                                    @elseif($data->status == 'rejected')
                                                        <button class="btn m-btn--pill btn-danger btn-sm m-btn m-btn--custom">
                                                    @elseif($data->status == 'dibatalkan')
                                                        <button class="btn m-btn--pill btn-warning btn-sm m-btn m-btn--custom">
                                                    @elseif($data->status == 'selesai')
                                                        <button class="btn m-btn--pill btn-info btn-sm m-btn m-btn--custom">        
                                                    @endif
                                                            {{ $data->status }}
                                                        </button>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="" type="button" class="btn btn-success" id="detail"
                                                    data-toggle="modal" 
                                                    data-target="#m_modal_3"
                                                    data-namaakun="{{ $data->nama_akun }}"
                                                    data-catatan="{{ $data->catatan }}">
                                                    Detail Jadwal
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="m_modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Informasi Akun Zoom
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>
                        Nama Akun
                    </h5>
                    <p>
                        <span id='namaakun'></span>
                    </p>
                    <hr>
                    <h5>
                        Catatan
                    </h5>
                    <p>
                        <span id='catatan'></span>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
