@extends('../componnents/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@endsection

@section('content')
    <div class="modal fade" id="m_modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Informasi Akun Zoom
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>
                        Popover in a modal
                    </h5>
                    <p>
                        This
                        <a href="#" role="button" data-toggle="m-popover" class="btn btn-secondary popover-test" title="Popover title" data-content="Popover body content is set in this attribute.">
                            button
                        </a>
                        triggers a popover on click.
                    </p>
                    <hr>
                    <h5>
                        Tooltips in a modal
                    </h5>
                    <p>
                        <a href="#" class="tooltip-test" data-toggle="m-tooltip" title="Tooltip">
                            This link
                        </a>
                        and
                        <a href="#" data-toggle="m-tooltip" class="tooltip-test" title="Tooltip">
                            that link
                        </a>
                        have tooltips on hover.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    {{-- <button type="button" class="btn btn-primary">
                        Save changes
                    </button> --}}
                </div>
            </div>
        </div>
    </div>
@endsection