<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mPeminjaman;
use App\Models\mZoom;

class jadwalController extends Controller
{
    public function index(){
        $peminjaman = mPeminjaman
            ::join('zoom', 'peminjaman.id_zoom', '=', 'zoom.id')
            ->select('zoom.*', 'peminjaman.*')
            ->where('status', 'approved')
            ->get(); 
        
        return view('jadwal.jadwalList', ['data_peminjaman'=>$peminjaman]);
    }

}
