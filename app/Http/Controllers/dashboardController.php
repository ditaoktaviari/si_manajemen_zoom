<?php

namespace App\Http\Controllers;

use App\Models\mZoom;
use App\Models\mPeminjaman;
use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function index(){
        $zoom = mZoom
        ::all()
        ->where('status_aktif', 'tidak aktif')
        ->count();

        $zoomaktif = mZoom
        ::all()
        ->where('status_aktif', 'aktif')
        ->count();

        $peminjaman = mPeminjaman::all()->count();

        $peminjamanapproved = mPeminjaman
        ::all()
        ->where('status', 'approved')
        ->count();

        return view('dashboard.dashboard', [
            'zoom'=>$zoom, 
            'zoomaktif'=>$zoomaktif, 
            'peminjaman'=>$peminjaman, 
            'peminjamanapproved'=>$peminjamanapproved
        ]);
    }

    public function stafindex(){
        $zoom = mZoom
        ::all()
        ->where('status_aktif', 'tidak aktif')
        ->count();
        $zoomaktif = mZoom
        ::all()
        ->where('status_aktif', 'aktif')
        ->count();
        $peminjaman = mPeminjaman::all()->count();
        $peminjamanapproved = mPeminjaman
        ::all()
        ->where('status', 'approved')
        ->count();

        return view('dashboard.stafdashboard', [
            'zoom'=>$zoom, 
            'zoomaktif'=>$zoomaktif, 
            'peminjaman'=>$peminjaman, 
            'peminjamanapproved'=>$peminjamanapproved
        ]);
    }
}
