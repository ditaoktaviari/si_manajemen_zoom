<?php

namespace App\Http\Controllers;

use App\Models\mPeminjaman;
use App\Models\mZoom;
use Illuminate\Http\Request;

class peminjamanController extends Controller
{
    public function index(){
        $peminjaman = mPeminjaman
            ::join('zoom', 'peminjaman.id_zoom', '=', 'zoom.id')
            ->select('zoom.*', 'peminjaman.*')
            ->get(); 
        
        return view('peminjaman.peminjamanList', ['data_peminjaman'=>$peminjaman]);
    }

    public function create(){
        $zoom = mZoom::all();
        return view('peminjaman.peminjamanCreate',['zoom'=>$zoom]);
    }

    public function insert(Request $request){
        $request->validate([
            'id_zoom' => 'required',
            'nama_kegiatan' => 'required',
            'deskripsi' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
        ]);

        $tanggal_pinjam = mPeminjaman::
            where('id_zoom', $request->id_zoom)
            ->whereBetween('tanggal_pinjam', [$request->tanggal_pinjam, $request->tanggal_kembali])
            ->first();

        if($tanggal_pinjam != null){
            return redirect('/createPeminjaman')->with('msg', 'Tanggal tersebut sudah Terjadwal');
        }

        $peminjaman = new mPeminjaman();
        $peminjaman->id_zoom = $request->id_zoom;
        $peminjaman->nama_kegiatan = $request->nama_kegiatan;
        $peminjaman->deskripsi = $request->deskripsi;
        $peminjaman->tanggal_pinjam = $request->tanggal_pinjam;
        $peminjaman->tanggal_kembali = $request->tanggal_kembali;

        
        $peminjaman->save();

        if($peminjaman)
            return redirect('/tampilPeminjaman')->with('sukses', 'Request Peminjaman Berhasil!');
        else
            return redirect('/tampilPeminjaman')->with('msg', 'Request Peminjaman Gagal!');
    }

    public function edit($id)
    {
        $peminjaman = mPeminjaman::where('id', $id)->first();
        $zoom = mZoom::all();

        $data = [
            'edit' => $peminjaman,
            'zoom' => $zoom,
        ];

        return view('peminjaman.peminjamanEdit', $data);
    }

    public function update(Request $request, $id){
        /* $request->validate([
            'id_zoom' => 'required',
            'nama_kegiatan' => 'required',
            'deskripsi' => 'required',
            'tanggal_pinjam' => 'required',
            'tanggal_kembali' => 'required',
        ]); */
        $request->validate([
            'catatan' => 'required',
        ]);
        
        $peminjaman = mPeminjaman::find($id);
        /* $peminjaman->id_zoom = $request->id_zoom;
        $peminjaman->nama_kegiatan = $request->nama_kegiatan;
        $peminjaman->deskripsi = $request->deskripsi;
        $peminjaman->tanggal_pinjam = strtotime($request->tanggal_pinjam);
        $peminjaman->tanggal_kembali = strtotime($request->tanggal_kembali); */
        $peminjaman->status = $request->status;
        $peminjaman->catatan = $request->catatan;

        $peminjaman->save();
        
        if($peminjaman)
            return redirect('/tampilPeminjaman')->with('sukses', 'Status Berhasil Diset!');
        else
            return redirect('/tampilPeminjaman')->with('msg', 'Status Gagal Diset!');
    }

    public function delete($id){
        $peminjaman = mPeminjaman::find($id);
        $status = mPeminjaman
            ::select('peminjaman.*')
            ->where('peminjaman.id', $id)
            ->where('peminjaman.status', 'approved')
            ->count();

        if ($peminjaman != null) {
            if ($status){
                return redirect('/tampilPeminjaman')->with('msg', 'Status Peminjaman Approved, Tidak Dapat Menghapus Request Peminjaman');
            }else{
                $peminjaman->delete();
                return redirect('/tampilPeminjaman')->with('sukses', 'Request Peminjaman Berhasil Dihapus!');
            }   
        }
    }
}