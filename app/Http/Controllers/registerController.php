<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class registerController extends Controller
{
    public function index(){
        return view('register.register');
    }
    public function create(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email:dns|unique:users',
            'role' => 'required',
            'password' => 'required|min:5',
            //'rpassword' => 'required|min:5',
        ]);
        
        //$validatedData['password'] = bcrypt($validatedData['password']);

        $validatedData['password'] = Hash::make($validatedData['password']);

        User::create($validatedData);

        //$request->session()->flash('sukses', 'Sign Up Berhasil, Silakan Sign In!');

        return redirect('/')->with('sukses', 'Sign Up Berhasil, Silakan Sign In!');
    }
}
