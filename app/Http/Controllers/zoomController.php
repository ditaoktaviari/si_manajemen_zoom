<?php

namespace App\Http\Controllers;
use App\Models\mZoom;

use Illuminate\Http\Request;

class zoomController extends Controller
{
    public function index(){
        $zoom = mZoom::all(); // menampilkan data tampa data yang di soft delete
        //$zoom = zoom::withTrashed()->get(); // menampilkan data yang sudah di soft delete
        return view('zoom.zoomList', ['data_zoom'=>$zoom]);
    }

    public function create(){
        return view('zoom.zoomCreate');
    }

    public function insert(Request $request){
        $request->validate([
            'nama_akun' => 'required',
            'kapasitas' => 'required',
            'status_aktif' => 'required',
        ]);

        $zoom = new mZoom();
        $zoom->nama_akun = $request->nama_akun;
        $zoom->kapasitas = $request->kapasitas;
        $zoom->status_aktif = $request->status_aktif;

        $zoom->save();

        return redirect('/tampilZoom')->with('sukses', 'Data berhasil tersimpan!');;
    }

    public function edit($id)
    {
        $zoom = mZoom::where('id', $id)->first();

        $data = [
            'edit' => $zoom
        ];

        return view('zoom.zoomEdit', $data);
    }

    public function update(Request $request, $id){
        $request->validate([
            'nama_akun' => 'required',
            'kapasitas' => 'required',
            'status_aktif' => 'required',
        ]);
        
        $zoom = mZoom::find($id);
        $zoom->nama_akun = $request->nama_akun;
        $zoom->kapasitas = $request->kapasitas;
        $zoom->status_aktif = $request->status_aktif;

        $zoom->save();

        return redirect('/tampilZoom')->with('sukses', 'Data berhasil diedit!');;
    }

    public function delete($id){
        $zoom = mZoom::find($id);
        if ($zoom != null) {
            $zoom->delete();
            // $zoom->forcedelete(); // data tetap terhapus walaupun menggunakan soft delete
            return redirect('/tampilZoom')->with('sukses', 'Data berhasil terhapus!');;
        }
    }
}
