<?php

namespace App\Http\Controllers;
use APP\Models\User;

use Illuminate\Http\Request;

class resetpassController extends Controller
{
    protected function redirectTo(){
        if(Auth()->user()->role == 1){
            return redirect()->route('stafdashboard');
        }else{
            return redirect()->route('dashboard');
        } 
    }
}
