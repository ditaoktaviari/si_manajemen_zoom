<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mPeminjaman extends Model
{   
    use SoftDeletes;
    protected $table = 'peminjaman';
    protected $fillable = [
        'id_zoom',
        'nama_kegiatan',
        'deskripsi',
        'tanggal_pinjam',
        'tanggal_kembali',
    ];

    function zoom(){
        return $this->belongsTo(mZoom::class, 'id_zoom', 'id');
    } 
}
