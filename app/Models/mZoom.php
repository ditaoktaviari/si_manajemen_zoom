<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class mZoom extends Model
{
    use SoftDeletes;
    protected $table = 'zoom';
    protected $guated = ['id'];
}
